package com.starter.demo.commands;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ProductCommand {
    private String label;
    private Integer price;
}
