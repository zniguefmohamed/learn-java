package com.starter.demo.API;

import com.starter.demo.Domain.Product;
import com.starter.demo.Service.product.ProductService;
import com.starter.demo.commands.ProductCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor // create constructors if needed
@RequestMapping("/api/v1/products") // ( api path /dalaha )
public class ProductRessource {

    private final ProductService productService;
    @GetMapping
    public ResponseEntity<List<Product>> getAllProduct() {
        return ResponseEntity.ok(productService.getAll());
    };

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable String id) {
        return ResponseEntity.ok(productService.getById(id));
    }

    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestBody ProductCommand productCommand) {
        return ResponseEntity.ok(productService.createProduct(productCommand));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable String id) {
        productService.delete(id);
        return ResponseEntity.noContent().build();
    }


}
