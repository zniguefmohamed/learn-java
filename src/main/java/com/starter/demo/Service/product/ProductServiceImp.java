package com.starter.demo.Service.product;

import com.starter.demo.Domain.Product;
import com.starter.demo.Repository.ProductRepository;
import com.starter.demo.commands.ProductCommand;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j // log terminal
public class ProductServiceImp implements ProductService{

    private final ProductRepository productRepository;
    @Override
    public List<Product> getAll() {
        log.info("Begin fetching all product");
        return productRepository.findAll(); // return [] of product
    }

    @Override
    public Product getById(String id) {
        log.info("Begin fetching product with id: {}", id);
        return productRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @Override
    public Product createProduct(ProductCommand productCommand) {
        Product product = Product.create(productCommand);
        Product newProduct = productRepository.save(product);
        log.info("product with id {} has been created", newProduct.getId());
        return newProduct;
    }

    @Override
    public void delete(String id) {
        productRepository.deleteById(id);
    }
}
