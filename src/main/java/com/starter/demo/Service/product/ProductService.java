package com.starter.demo.Service.product;

import com.starter.demo.Domain.Product;
import com.starter.demo.commands.ProductCommand;

import java.util.List;

public interface ProductService {
    List<Product> getAll();
    Product getById(String id);
    Product createProduct(ProductCommand productCommand);
    void delete(String id);
}
