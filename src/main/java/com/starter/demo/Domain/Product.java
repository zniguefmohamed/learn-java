package com.starter.demo.Domain;

import com.starter.demo.commands.ProductCommand;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Table // create table ( name: take default class name )
@Entity // create table entity
@NoArgsConstructor //
@AllArgsConstructor //
@Getter //
@Setter //
public class Product {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "ID")
    @EqualsAndHashCode.Include
    private String id;

    @Column
    private String label;

    @Column
    private Integer price;

    public static Product create(ProductCommand productCommand) {
        Product product = new Product();
        product.setLabel(productCommand.getLabel());
        product.setPrice(productCommand.getPrice());

        return product;
    }
}
